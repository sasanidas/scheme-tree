;;; scheme-tree.el ---  scheme-tree            -*- lexical-binding: t; -*-

;; trace: |  (cc 1 4)
;; trace: |  |  (cc 1 3)
;; trace: |  |  |  (cc 1 2)
;; trace: |  |  |  |  (cc 1 1)
;; trace: |  |  |  |  |  (cc 1 0)
;; trace: |  |  |  |  |  0
;; trace: |  |  |  |  |  (first-denomination 1)
;; trace: |  |  |  |  |  1
;; trace: |  |  |  |  |  (cc 0 1)
;; trace: |  |  |  |  |  1
;; trace: |  |  |  |  1
;; trace: |  |  |  |  (first-denomination 2)
;; trace: |  |  |  |  5
;; trace: |  |  |  |  (cc -4 2)
;; trace: |  |  |  |  0
;; trace: |  |  |  1
;; trace: |  |  |  (first-denomination 3)
;; trace: |  |  |  10
;; trace: |  |  |  (cc -9 3)
;; trace: |  |  |  0
;; trace: |  |  1
;; trace: |  |  (first-denomination 4)
;; trace: |  |  25
;; trace: |  |  (cc -24 4)
;; trace: |  |  0
;; trace: |  1




;; Copyright (C) 2021 Fermin Munoz

;; Author: Fermin Munoz
;; Maintainer: Fermin Munoz <fmfs@posteo.net>
;;             David Andreu <davidandreu@posteo.net>
;; Created: 05 Feb 2021
;; Version: 0.0.1
;; Keywords: 
;; URL: 
;; Package-Requires: ((emacs "26.3"))
;; License: GPL-3.0-or-later

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Quick intro
;;


;; Functions for buffer movement

;; next-line
;; beginning-of-line
;; buffer-substring-no-properties
;; point-max
;; point-min
;; point
;; insert
;; rx

;;; Code:

;;;; The requires

(require 'seq)
(eval-when-compile
  (require 'cl-lib))


(defvar scheme-tree-buffer-name "* Guile REPL *")

(defvar scheme-tree--sleep-time 0.5)

(cl-defun scheme-tree--send-string (string process)
  "Send STRING to PROCESS.
Optionally you can pass a CODE to execute."
  (with-current-buffer (process-buffer process)
    (insert string)
    (comint-send-input)))



(cl-defun scheme-tree--preprocess (string)
  "Preprocess the output of the function to fit.
The STRING parameter."
  (let* ((regex "trace:")
	 (current-point nil))
    (with-temp-buffer
      (insert string)
      (goto-char (point-min))
      (while (re-search-forward regex nil t)
	(setq current-point (point))
	(beginning-of-line)
	(delete-region (point) current-point))
      (buffer-string))))


(cl-defun scheme-tree-get-output (&key code (process scheme-tree-buffer-name))
  "Get the output form the guile process.
It requires a PROCESS and the function CODE."
  (if (equal major-mode 'scheme-mode)
      (let* ((cmdlist (split-string-and-unquote "guile"))
	     (guile-buffer
	      (apply 'make-comint (generate-new-buffer-name "scheme") (car cmdlist)
		     (scheme-start-file (car cmdlist)) (cdr cmdlist)))
	     (guile-process (get-buffer-process guile-buffer ))
	     (current-cbuffer (buffer-string)))

	(scheme-tree--send-string (with-temp-buffer
			 (insert current-cbuffer)
			 (delete-indentation)
			 (buffer-string))
		       guile-process)

	(sleep-for scheme-tree--sleep-time)
	(with-current-buffer guile-buffer
	  (insert (format ",trace %s" code))
	  (comint-send-input)
	  (let* ((buffer-point (comint-show-output)))
	    (sleep-for scheme-tree--sleep-time)
	    (buffer-substring-no-properties buffer-point (point-max)))))
    (error "Me estas jodiendo")))





(provide 'scheme-tree)
;;; scheme-tree.el ends here
