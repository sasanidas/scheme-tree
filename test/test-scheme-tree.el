;;; test-scheme-tree.el --- scheme-tree test file.               -*- lexical-binding: t; -*-

;; Copyright (C) 20 Fermin Munoz

;; License: GPL-3.0-or-later

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;; The requires
(require 'ert)

(ert-deftest scheme-tree-get-output ()
  (should (string= "trace: |  3 scheme@(guile-user)> "
		   (with-temp-buffer
		     (scheme-mode)
		     (insert (scheme-tree-get-output :code "3"))
		     (delete-indentation)
		     (buffer-string)))))

(ert-deftest scheme-tree-eval-content ()
  (should (equal '("trace:" "|" "(sumar" "3" "2)" "trace:" "|" "5" "scheme@(guile-user)>")
		 (with-temp-buffer
		   (scheme-mode)
		   (insert
		    "(define (sumar a b) (+ a b))")
		   (split-string (scheme-tree-get-output :code "(sumar 3 2)")))
		 )))
(ert t)

(provide 'test-scheme-tree.el)
;;; test-scheme-tree.el ends here
